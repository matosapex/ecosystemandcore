МІНІСТЕРСТВО ОСВІТИ І НАУКИ УКРАЇНИ
ХАРКІВСЬКИЙ НАЦІОНАЛЬНИЙ ЕКОНОМІЧНИЙ УНІВЕРСИТЕТ
ІМЕНІ СЕМЕНА КУЗНЕЦЯ

ЗВІТ
з дисципліни «Сучасні Java-технології»
за  курсом «1 тиждень - Spring - Ecosystem and Core»

Виконав:
Студент 4 курсу
групи 6.04.122.010.19.3
факультету ІТ
Григоров М.В.

Перевірив:
Фролов О.В.
Поляков А.О.

Харків – 2022

___

### URL – адреса сертифікату
https://www.coursera.org/verify/MANK9EUN7E4G

___

### Картинка сертифікату
![](img/img1.png)
Рис.1. Сертифікат
___

### Скриншот з профілю Coursera, що відображуе отриманий бал за курс
![](img/img4.png)
Рис.2. Скриншот з профілю Cursera, що відображуе отриманий бал за курс
___
## Початок курсу  Spring - Ecosystem and Core
## 1 Тиждень

![](img/img2.png)
Рис.3. Скриншот з 1 тижня курсу Spring - Ecosystem and Core.

![](img/img5.png)
Рис.4. Скриншот з 1 тижня курсу Spring - Ecosystem and Core.

![](img/img3.png)
Рис.5. Скриншот з оцінуою за фінальний тест 1 тиждень
___
